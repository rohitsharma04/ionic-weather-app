weatherApp.config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider) {
	$stateProvider
		.state('app',{
			url: '/app',
			abstract: true,
			templateUrl: 'templates/menu.html',
			controller: 'MenuCtrl'
		})
		.state('app.weather',{
			url : '/weather',
			views:{
				'menuContent':{
					templateUrl: 'templates/weather.html',
					controller: 'WeatherCtrl'
				}
			}
		});

		$urlRouterProvider.otherwise('/app/weather');	
}]);