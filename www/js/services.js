weatherApp.factory('Settings', ['', function(){
	var Settings = {
		units: 'us'
	};
	return Settings;
}]);


weatherApp.factory('WeatherService', ['$http','$log', function($http, $log){
	$log.info('Weather Factory');
	var API_KEY = 'bc6b76631c1b12e8f16b5e03e4d78d14';
	var API_URL = 'http://api.openweathermap.org/data/2.5/weather?appid='+API_KEY;
	// var city = 'howrah';
	return {
		weatherDataByCity : function(city){
			return $http.get(API_URL+'&q='+ city);
		},
		weatherDataByGeoLocation : function(lon, lat){
			return $http.get(API_URL+'&lat='+lat+'&lon='+lon);
		}
	}
}]);

//Location factory
weatherApp.factory('LocationService', ['$log', function($log){
	$log.info('LocationService Factory');
	var LocationService = {
		lat : 0,
		lon: 0,
		city: 'Kolkata',
		gps: false
	};
	return LocationService;
}]);
